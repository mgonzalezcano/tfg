Template.adminpanel.rendered = function() {
  Session.setDefault('gestuseractivo', false);
  Session.setDefault('gesttfmactivo', false);



};



Template.adminpanel.helpers({
  rendered: function(){
  },
  us : function () {
    return Meteor.users.find({});
  },

  validado: function (state) {
    if (state ==="Validado"){
      return true
    }else{
      return false
    }
  },

  guactivado: function(){
   return Session.get("gestuseractivo");
  },
  gtactivado: function(){
  return Session.get("gesttfmactivo");
  }
});




Template.adminpanel.events({
  "click #boton-valida": function (e,t) {
    var selected = $("ui.button").find("active");
    var usr = $(e.target).attr('user');
    console.log(usr);
    Meteor.call("validar_usuario", usr);
  },
  "click #gestuser": function (e) {
    Router.go('/admin2/gestiontfm');
    if($('#gesttfm').hasClass('active')){
      $('#gesttfm').removeClass('active');
      Session.set('gesttfmactivo', false);
    }
    $('#gestuser').addClass('active');
    Session.set('gestuseractivo', true);
    console.log("gest user");

  },
  "click #gesttfm": function (e) {
    Router.go('/admin2/gestionusuarios');
    if($('#gestuser').hasClass('active')){
      Session.set('gestuseractivo', false);
      $('#gestuser').removeClass('active');
    }
    $('#gesttfm').addClass('active');
    Session.set('gesttfmactivo', true);
    console.log("gest tfm");

  }

});



Template.up.helpers({
  userSchema: function () {
    return Schema.User;
  },
  user: function() {
    return Meteor.user();
  }
});
/*
Template.select.helpers({
  options: function () {
    return [{label:"Validado", value:"Validado"}, {label:"Pendiente", value:"Pendiente"}]
  }
});
*/




/*


'keypress .adminusersbar .search-query': function(event) {
  if (event.charCode == 13) {
alert('you hit enter');
event.stopPropagation();
return false;
  }
},
'keyup .adminusersbar .search-query': function(event, template) {
  clearTimeout(timerid);
  timerid = setTimeout(function() {
var search = template.find(".search-query").value;
Session.set("userFilter", search);
    }, 1000);
  return false;
},

*/



var HooksObject = {
  after: {
    update: function(error, result) {
      if (!error) {
        Router.go('/');
      }
    }
  },
  onSubmit: function(insertDoc, updateDoc, currentDoc) {
    // You must call this.done()!
    this.done(); // submitted successfully, call onSuccess
    //this.done(new Error('foo')); // failed to submit, call onError with the provided error
    //this.done(null, "foo"); // submitted successfully, call onSuccess with `result` arg set to "foo"
  }
};



AutoForm.hooks({
  profileUpdateForm: HooksObject
});
