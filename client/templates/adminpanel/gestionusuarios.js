Template.gusers.rendered = function() {
  Session.setDefault('gestuseractivo', true);
  Session.setDefault('gesttfmactivo', false);
  $('#gestuser').addClass('active');
  $('#gesttfm').removeClass('active');

};

Template.gusers.helpers({
  rendered: function(){


  },
  us : function () {
    return Meteor.users.find({});
  },
  matrizroles : function(){
    var allowedRoles = [
      {"name" : "admin"},
      {"name" : "coordinador"},
      {"name" : "tutor"},
      {"name" : "comision"},
      {"name" : "alumno"}
    ];
    return allowedRoles;
  },
  mroles : function(){
    return this.roles;
  },
  isInRole : function(user,rol){
    return Roles.userIsInRole(user,rol);
  },
  valor : function(){

  },
  isChecked : function(user,rol){
    return Roles.userIsInRole(user,rol);
  },
  validado: function (state) {
    if (state ==="Validado"){
      return true
    }else{
      return false
    }
  }
});

Template.gusers.events({
  'change .userRolesInput': function(event, tmpl) {
    console.log("click");
  },
  'click #actualizar' : function (event) {
      var usr = $(event.target).attr('user');
      var t = "tutor_" + usr;
      var a = "alumno_" + usr;
      var c = "comision_" + usr;
      var checkboxt = document.getElementsByName(t)[0].checked;
      var checkboxa = document.getElementsByName(a)[0].checked;
      var checkboxc = document.getElementsByName(c)[0].checked;
      var roles = [];
      if(checkboxt) roles.push("tutor");
      if(checkboxa) roles.push("alumno");
      if(checkboxc) roles.push("comision");

      var r = confirm("¿Está seguro de actualizar los roles seleccionados?");
      if(r){
        Meteor.call("actualizar_roles",usr,roles);
        location.reload();
        alert("Roles cambiados");
      }else{
        location.reload();
      }
  },
  "click #boton-valida": function (e,t) {
    var selected = $("ui.button").find("active");
    var usr = $(e.target).attr('user');
    console.log(usr);
    Meteor.call("validar_usuario", usr);
  }
});
