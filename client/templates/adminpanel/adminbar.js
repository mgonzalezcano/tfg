Template.adminbar.events({
  "click #gestuser": function (e) {
    if($('#gesttfm').hasClass('active')){
      $('#gesttfm').removeClass('active');
      Session.set('gesttfmactivo', false);
    }
    $('#gestuser').addClass('active');
    Session.set('gestuseractivo', true);
    console.log("gest user");
    Router.go('/admin2/gestionusuarios');
  },
  "click #gesttfm": function (e) {
    if($('#gestuser').hasClass('active')){
      Session.set('gestuseractivo', false);
      $('#gestuser').removeClass('active');
    }
    $('#gesttfm').addClass('active');
    Session.set('gesttfmactivo', true);
    console.log("gest tfm");
    Router.go('/admin2/gestiontfm');
  }
});
