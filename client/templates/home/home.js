if(Meteor.isClient){
    Meteor.startup(function(){
        Hooks.init();
    });
}

Template.home.rendered = function() {
  //Meteor.subscribe('usuarios');

};

Template.home.helpers({
  email : function () {
    var us = Meteor.user();
    return us.emails[0].address;
  }
});


Template.home.events({
  'click #btnsave' : function () {
    $('.ui.sidebar').sidebar('toggle');

  },
  'click #btndiscard' : function () {
    var usuario = Meteor.user();
    Meteor.call("sendEmailPrueba",usuario.emails[0].address, function(error){
      if(error){
        console.log("error", error);
      }
    });


  }
});
