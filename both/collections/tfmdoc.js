Tfmdoc = new Mongo.Collection('tfmdoc');

Tfmdoc.helpers({

});



var Schemas = {};

Schemas.Tfmdoc = new SimpleSchema({
  titulo: {
    type: String,
    label: "Titulo",
    max: 200
  },
  tipo: {
    type: String,
    label: "Autor",
  },
  resumen: {
    type: String,
    label: "Resumen",
    max: 1000
  },
  idioma: {
    type: String,
    label: "Idioma",
  },
  area: {
    type: String,
    label: "Area de Conocimiento",
  },
  competencias: {
    type: [Boolean],
    label: "Competencias",
    optional: true
  }

});

Tfmdoc.attachSchema(Schemas.Tfmdoc);
