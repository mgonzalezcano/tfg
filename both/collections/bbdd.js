/*USUARIOS*/
var Schema = {};



Schema.UserCountry = new SimpleSchema({
    name: {
        type: String
    },
    code: {
        type: String,
        regEx: /^[A-Z]{2}$/
    }
});

Schema.UserProfile = new SimpleSchema({
    firstName: {
        type: String,
        regEx: /^[a-zA-Z-]{2,25}$/,
        optional: true
    },
    lastName: {
        type: String,
        regEx: /^[a-zA-Z\s]{2,25}$/,
        optional: true
    },
    birthday: {
        type: Date,
        optional: true
    },
    gender: {
        type: String,
        allowedValues: ['Male', 'Female'],
        optional: true
    },
    organization : {
        type: String,
        regEx: /^[a-z0-9A-z .]{3,30}$/,
        optional: true
    },
    website: {
        type: String,
        regEx: SimpleSchema.RegEx.Url,
        optional: true
    },
    country: {
        type: Schema.UserCountry,
        optional: true
    }
});

Schema.User = new SimpleSchema({
    username: {
        type: String,
        regEx: /^[a-z0-9A-Z_]{3,15}$/,
        optional: true
    },
    emails: {
        type: [Object]
        // this must be optional if you also use other login services like facebook,
        // but if you use only accounts-password, then it can be required

    },
    "emails.$.address": {
        type: String,
        regEx: SimpleSchema.RegEx.Email
    },
    "emails.$.verified": {
        type: Boolean
    },

    createdAt: {
        type: Date
    },
    profile: {
        type: Schema.UserProfile,
        optional: true
    },

    services: {
        type: Object,
        optional: true,
        blackbox: true
    },

    // Add `roles` to your schema if you use the meteor-roles package.
    // Option 1: Object type
    // If you specify that type as Object, you must also specify the
    // `Roles.GLOBAL_GROUP` group whenever you add a user to a role.
    // Example:
    // Roles.addUsersToRoles(userId, ["admin"], Roles.GLOBAL_GROUP);
    // You can't mix and match adding with and without a group since
    // you will fail validation in some cases.
    //roles: {
    //    type: Object,
      //  optional: true,
        //blackbox: true
    //},
    // Option 2: [String] type
    // If you are sure you will never need to use role groups, then
    // you can specify [String] as the type
    roles: {
        type: [String],
        optional: true,
        allowedValues : ['admin','tutor','comision','alumno','coordinador']
        /*
        autoValue: function (){
          if(this.isInsert && this.field === ""){
            return 'alumno';
          }

        }*/
    },
    estado: {
      type: String,
      allowedValues: ['Validado','Pendiente',],
      autoValue: function () {
        if(this.isInsert){
          return "Pendiente";
        }
      },
      optional: true
    }
});

Meteor.users.attachSchema(Schema.User);

Meteor.users.before.insert(function (userId, doc) {

});
/* --------------------------------------------------   TFM ------------------------------------------------ */
/* --------------------------------------------------
/* --------------------------------------------------
/* -------------------------------------------------- */


PropuestaTfm = new Mongo.Collection('propuestatfm');



Schema.DatosAlumno = new SimpleSchema({
  alumno:{
    type: String,
    label: "nombre usuario alumno",
    autoform:{
      type: "select",
      options: function (){
        return Meteor.users.find().map(function (c) {
          return {label: c.emails[0].address, value: c._id};
        });
      }
    }
  },
  ampliaciontfm: {
    type: Boolean,
    label: "Es ampliación de TFM"
  }

});


Schema.DatosTutor = new SimpleSchema({
  tutor: {
    type: String,
    label: "Tutor",
    autoform:{
      type: "select",
      options: function (){
        return Meteor.users.find().map(function (c) {
          return {label: c.emails[0].address, value: c._id};
        });
      }
    }
  },
  area: {
    type: String,
    label: "Area de Conocimiento",
  },
  organizacion: {
    type: String,
    label: "organizacion"
  },
  cotutor:{
    type: String,
    label: "co-tutor",
    optional: true,
    autoform:{
      type: "select",
      options: function (){
        return Meteor.users.find().map(function (c) {
          return {label: c.emails[0].address, value: c._id};
        });
      }
    }
  }
});

Schema.DatosTfm = new SimpleSchema({
  titulo: {
    type: String,
    label: "titulo del tfm"
  },
  tipo: {
    type: String,
    allowedValues: ['Desarrollo','Estudio']
  },
  resumen: {
    type: String,
    label: "resumen del tfm"
  },
  idioma: {
    type: String,
    label: "idioma",
    allowedValues: ['Castellano','Ingles','A elegir']
  },
  area: {
    type: String,
    label: "area de conocimiento",
    allowedValues: ['LSI','ATC','OE','MA']
  },
  competencias: {
    type: [Object],
    label: "competencias",
  },
  "competencias.$.id": {
    type: [String],
    allowedValues:['CE1','CE2','CE3','CE4','CE5','CE6','CE7','CE8','CE9','CE10','CE11','CE12','CE13','CE14','CE15'],
    autoform:{
      type: "select-checkbox"
    }
  }

});

Schema.Competencias = new SimpleSchema({
  id : {
    type: [String],
    allowedValues: ['CE1','CE2','CE3','CE4','CE5','CE6','CE7','CE8','CE9','CE10','CE11','CE12','CE13','CE14','CE15'],
    label: "Competencias",
    autoform: {
      type: "select2",
      multiple: true,
    }
  }

});


Schema.PropuestaTfm = new SimpleSchema({

  datostutor: {
    type: Schema.DatosTutor,
    label: "datos del tutor"
  },

  datostfm: {
    type: Schema.DatosTfm,
    label: "datos del tfm"
  },

  datosalumno: {
    type: Schema.DatosAlumno,
    optional: true
  }



});
Meteor.users.allow({
    insert: function () { return true; },
    update: function () { return true; },
    remove: function () { return true; }
});

PropuestaTfm.attachSchema(Schema.PropuestaTfm);
PropuestaTfm.before.insert(function (userId, doc){
  doc.createdAt = Date.now();
});
