/*PropuestaTfm = new Mongo.Collection('propuestatfm');

PropuestaTfm.helpers({

});


var Schemas = {};

Schemas.PropuestaTfm = new SimpleSchema({

  datostutor:{
    type: Schemas.Datostutor,
    label: "datos del tutor"

  },

  datostfm:{
    type: Schemas.DatosTfm,
    label: "datos del tfm"
  },

  datosalumno:{
    type: Schemas.DatosAlumno,
    optional: true
  }

});

Schemas.Datosalumno = new SimpleSchema({
  alumno:{
    type: Schema.User
  },
  ampliaciontfm: {
    type: Boolean
  }

});


Schemas.Datostutor = new SimpleSchema({
  tutor: {
    type: Schema.User,
    label: "Tutor"
  },
  area: {
    type: String,
    label: "Area de Conocimiento",
  },
  organizacion: {
    type: String,
    label: "organizacion"
  },
  cotutor:{
    type: String,
    label: "co-tutor",
    optional: true
  }
});

Schemas.DatosTfm = new SimpleSchema({
  titulo: {
    type: String,
    label: "titulo del tfm"
  },
  tipo: {
    type: String,
    allowedValues: ['Desarrollo','Estudio']
  },
  resumen: {
    type: String,
    label: "resumen del tfm"
  },
  idioma: {
    type: String,
    label: "idioma",
    allowedValues: ['Castellano','Ingles','A elegir']
  },
  area: {
    type: String,
    label: "area de conocimiento",
    allowedValues: ['LSI','ATC','OE','MA']
  },
  competencias: {
    type: Schemas.Competencias,
    label: "competencias"
  }

});

Schemas.Competencias = new SimpleSchema({
  id :{
    type: String,
    allowedValues: ['CE1','CE2','CE3','CE4','CE5','CE6','CE7','CE8','CE9','CE10','CE11','CE12','CE13','CE14','CE15']
  }
})

PropuestaTfm.attachSchema(Schemas.PropuestaTfm);
*/
