//AccountsTemplates.configure({forbidClientAccountCreation: true});
AdminConfig = {
  name: 'iTFM',
  adminEmails: ['zdezae@gmail.com']

}





AccountsTemplates.configureRoute('signIn', {layoutTemplate: 'appLayout'});
AccountsTemplates.configureRoute('signUp', {layoutTemplate: 'appLayout'});
AccountsTemplates.configureRoute('ensureSignedIn', {layoutTemplate: 'appLayout'});

var pwd = AccountsTemplates.removeField('password');
AccountsTemplates.removeField('email');
AccountsTemplates.addFields([
  {
  _id: 'email',
  type: 'email',
  required: true,
  displayName: "Direccion email",
},
  {

    _id: 'rol',
    type: "select",
    displayName: "roles?",
    select: [

      {
        text: "alumno",
        value: "alumno",
      },{
        text: "tutor",
        value: "tutor",
      },
    ],

  }, pwd
]);


/*
var mySubmitFunc = function(error, state){
  if (!error) {
    if (state === "signUp") {
        console.log("Nuevo USUARIO!");
      var usuario = Meteor.user();

      Meteor.call("sendEmailPrueba",usuario.emails[0].address, function(error){
        if(error){
          console.log("error", error);
        }
      });
    }


  }

  //      this.Meteor.user.password = "nuevapass1";

};

Accounts.createUser = _.wrap(Accounts.createUser, function(createUser) {

  // Store the original arguments
  var args = _.toArray(arguments).slice(1),
      user = args[0],
      origCallback = args[1];

  // Create a new callback function
  // Could also be defined elsewhere outside of this wrapped function
  // This is called on the client
  var newCallback = function(err) {

    var usuario = Meteor.user();
    var llamada = Meteor.call("sendEmailPrueba","hoola");


  };

  // Now call the original create user function with
  // the original user object plus the new callback
  createUser(user, newCallback);

});

*/



/*
Accounts.onCreateUser(function(options, user){
  var roles = options.profile.roles;
  user.roles = roles.split();
  return user;

});
*/

AccountsTemplates.configure({


  texts: {
        navSignIn: "Entrar",
        navSignOut: "Salir",
        optionalField: "Opcional",
        pwdLink_pre: "",
        pwdLink_link: "Contraseña olvidada",
        pwdLink_suff: "",
        resendVerificationEmailLink_pre: "Verification email lost?",
        resendVerificationEmailLink_link: "Send again",
        resendVerificationEmailLink_suff: "",
        sep: "OR",
        signInLink_pre: "Si ya tienes una cuenta -> ",
        signInLink_link: "Entrar",
        signInLink_suff: "",
        signUpLink_pre: "dontHaveAnAccount",
        signUpLink_link: "Registrate",
        signUpLink_suff: "",
        socialAdd: "add",
        socialConfigure: "Configuracion",
        socialIcons: {
            "meteor-developer": "fa fa-rocket",
        },
        socialRemove: "Eliminar",
        socialSignIn: "Entrar",
        socialSignUp: "Registrarse",
        socialWith: "with",
        termsPreamble: "clickAgree",
        termsPrivacy: "privacyPolicy",
        termsAnd: "and",
        termsTerms: "terms",
      title: {
        changePwd: "Password Title",
        enrollAccount: "Enroll Title",
        forgotPwd: "Forgot Pwd Title",
        resetPwd: "Reset Pwd Title",
        signIn: "Accede a la aplicación",
        signUp: "Registrate!",
        verifyEmail: "Verify Email Title",
      }
    }
});
