Meteor.methods({
  validar_usuario : function(userId){
    check(userId, String);
      var usuario = Meteor.users.find({ _id:userId});
      Meteor.users.update({_id:userId}, {$set:{
        estado : "Validado"

      }});


  },
  actualizar_roles : function(userId,roles){
    check(userId,String);
    check(roles,[Match.Any]);
    Roles.setUserRoles(userId,roles);

  },
  eventsOnHooksInit : function(){}
});
